const express = require('express')
// const sass = require('node-sass')
const app = express()
const port = 3000

app.set('view engine', 'pug');
app.engine('pug', require('pug').__express)

bikedata = require('./bikes.json')

app.get('/',function(req,res) {
  res.render('home', {
    title: 'Bike Shops',
    allBikeData: bikedata
  });
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))